package api

import (
	"encoding/json"
	"net/http"

	"git.froth.zone/sam/awl/logawl"
	"git.froth.zone/sam/awl/query"
	"git.froth.zone/sam/awl/util"
	"github.com/go-chi/chi/v5"
)

// V1 is the handler for version 1 of the official(tm) awl API.
func V1() http.Handler {
	r := chi.NewRouter()

	r.Post("/placeholder", createQuery)

	return r
}

// @Summary Create (and execute) a DNS query
// @ID make-query
// @Param request body util.Request true "query params"
// @Produce json
// @Success 200 {object} util.Response
// @Router /api/v1/placeholder [post]
// .
func createQuery(w http.ResponseWriter, r *http.Request) {
	o := util.Options{
		Logger: logawl.New(),
		JSON:   true,
	}

	w.Header().Set("Content-Type", "application/json")

	if err := json.NewDecoder(r.Body).Decode(&o); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)

		return
	}

	answer, err := query.CreateQuery(o)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	res, err := query.PrintSpecial(answer, o)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	if _, err := w.Write([]byte(res)); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}
}
