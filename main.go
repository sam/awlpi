// package main is the main package.
package main

import (
	"errors"
	"flag"
	"fmt"
	"net/http"
	"time"

	"git.froth.zone/sam/awl/logawl"
	"git.froth.zone/sam/awlpi/api"
	_ "git.froth.zone/sam/awlpi/docs"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/pseidemann/finish"
	httpSwagger "github.com/swaggo/http-swagger"
)

var port = flag.Int("p", 8000, "Port to listen on")

func main() {
	flag.Parse()

	log := logawl.New()

	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(middleware.GetHead)
	r.Use(middleware.Recoverer)

	r.Get("/swagger", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/swagger/index.html", 301)
	})
	r.Mount("/swagger/", httpSwagger.WrapHandler)
	r.Mount("/api/v1", api.V1())

	fileServer := http.FileServer(http.Dir("public/"))
	r.Handle("/*", http.Handler(fileServer))

	srv := &http.Server{
		Addr:              fmt.Sprintf(":%d", *port),
		Handler:           r,
		ReadHeaderTimeout: 30 * time.Second,
	}

	fin := finish.Finisher{Log: log}
	fin.Add(srv)

	go func() {
		fmt.Println("Listening on port", *port)

		err := srv.ListenAndServe()
		if !errors.Is(err, http.ErrServerClosed) {
			log.Error(err)
		}
	}()

	fin.Wait()
}
